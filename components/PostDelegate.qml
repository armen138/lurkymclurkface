import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents

Component {
    id: postDelegate
    Kirigami.BasicListItem {
        id: listItem
        contentItem: RowLayout {
            Rectangle {
                width: 64
                height: 64
                Image {
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit
                    source: (model && model.thumbnail) ? model.thumbnail : "../icons/person.png"
                }
            }
            ColumnLayout {
                RowLayout {
                    Controls.Label {
                        Layout.fillWidth: true
                        height: Kirigami.Units.iconSizes.smallMedium
                        wrapMode: Text.WordWrap
                        text: model && model.title || "title"
                        color: listItem.checked || (listItem.pressed && !listItem.checked && !listItem.sectionDelegate) ? listItem.activeTextColor : listItem.textColor
                    }
                }
                RowLayout {
                    id: row_layout
                    Controls.ToolButton {
                        // Layout.fillWidth: true
                        height: Kirigami.Units.iconSizes.smallMedium
                        text: "r/" + ( model && model.subreddit) || "subreddit"
                        font.pointSize: 8
                        onClicked: root.set_subreddit(model.subreddit)
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    Controls.ToolButton {
                        onClicked: model.submit()
                        text: model.score
                        icon.source: "../icons/chevron-top.svg"
                        font.pointSize: 8
                    }
                    Controls.ToolButton {
                        onClicked: model.submit()
                        text: ( model && model.num_comments || 0)
                        icon.source: "../icons/comment-square.svg"
                        font.pointSize: 8
                    }
                }
            }
        }
        Component.onCompleted: function() {
            console.log(listItem.height);
        }
        // onClicked: showPassiveNotification("preview")
        onClicked: root.set_post(model) //previewSheet.show(model)
    }
}
