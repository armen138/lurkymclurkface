import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtMultimedia 5.12
// import QtWebKit 3.0
// import QtWebView 1.14
import QtWebEngine 1.10
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "../mjs/moment.mjs" as Moment

Kirigami.OverlaySheet {
    id: previewSheet
    property string post
    property string url
    header: ColumnLayout { 
        Controls.Label {
            id: subredditLabel
            font.pointSize: 8
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: qsTr("r/")
        }        
        Kirigami.Heading {
            Layout.fillWidth: true
            id: post_title
            text: qsTr("Title")
            wrapMode: Text.WordWrap        
        }
        Controls.Label {
            id: authorLabel
            font.pointSize: 8
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: qsTr("u/")
        }         
    }
    function set_preview(reddit_post) {
        post_title.text = reddit_post.title;
        subredditLabel.text = `r/${reddit_post.subreddit} - ${reddit_post.domain}`;
        let timestamp = Moment.moment(reddit_post.created * 1000).fromNow()
        authorLabel.text = `u/${reddit_post.author} - ${timestamp}`;
        url = reddit_post.url;
        imageBox.visible = false;
        selfText.visible = false;
        webViewer.visible = false;
        webViewer.url = "about:blank";

        // console.log(reddit_post.post_hint);
        if(reddit_post.selftext && reddit_post.post_hint !== "") {
                selfText.text = qsTr(reddit_post.selftext);
                selfText.visible = true;
        }
        switch(reddit_post.post_hint) {
            case "image":
                imagePost.source = reddit_post.url;
                imageBox.visible = true;
                break;
            case "self":
                selfText.text = qsTr(reddit_post.selftext);
                selfText.visible = true;
                break;
            case "rich:video":
                // console.log(reddit_post.url);
                if(reddit_post.youtube_video) {
                    webViewer.url = "../html/yt_player.html?" + reddit_post.youtube_video;
                }
                if(reddit_post.gfy_embed) {
                    webViewer.url = reddit_post.gfy_embed;
                }
                webViewer.visible = true;
                break;
            case "hosted:video":
                webViewer.url = reddit_post.video;
                webViewer.visible = true;
                break;
            case "link":
                if (reddit_post.tweet) {
                    webViewer.loadHtml(reddit_post.tweet);
                    try {
                        webViewer.runJavaScript("window.innerHeight", data => {
                            console.log(data);
                        });
                    } catch(e) {
                        console.log(e);
                    }
                    webViewer.visible = true;
                    webViewer.Layout.preferredHeight = reddit_post.height;
                    break;
                } 
                // else {
                //     print(JSON.stringify(reddit_post));
                // }               
            default:
                break;
                // imagePost.source = reddit_post.thumbnail;
                // imageBox.visible = true;
        }
    }
    function show(reddit_post) {
        open();
    }
    ColumnLayout {
        spacing: Kirigami.Units.largeSpacing * 5
        Layout.preferredWidth:  Kirigami.Units.gridUnit * 25        

        Rectangle {
            id: imageBox
            Layout.fillWidth: true
            visible: false
            // height: 400
            Layout.preferredHeight: 400
            Image {  
                id: imagePost
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                onStatusChanged: {
                    if (imagePost.status == Image.Ready) {     
                        let contentHeight = Math.floor(imagePost.sourceSize.height * (imagePost.width / imagePost.sourceSize.width));
                        parent.Layout.preferredHeight = contentHeight;
                        print(contentHeight);
                        imagePost.visible = true;
                    }
                }                
            }      
        }
        Controls.Label {
            visible: false
            id: selfText
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: ""
        }
        WebEngineView {
            id: webViewer
            visible: false
            Layout.fillWidth: true
            height: 400
            onLoadingChanged: {
                switch (loadRequest.status)
                {
                case WebEngineView.LoadSucceededStatus:
                    // parent.Layout.preferredHeight = 400;
                    webViewer.Layout.preferredHeight = 400;
                    print(webViewer.contentsSize);
                    webViewer.runJavaScript("document.querySelector('iframe').getAttribute('height')", function(result) { console.log("WINDOWHEIGHT " + result); });

                    break;
                case WebEngineView.LoadStartedStatus:
                case WebEngineView.LoadStoppedStatus:
                    break
                case WebEngineView.LoadFailedStatus:
                    selfText.text = "Failed to load the requested video"
                    break
                }
            }                
        }            
    }
}