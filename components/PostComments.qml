import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
// import QtWebKit 3.0
import QtMultimedia 5.12
import QtWebEngine 1.10
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents
import "../mjs/listing.mjs" as RedditListing

Kirigami.ScrollablePage {
    id: post_page
    property string url
    property string post
    property string video
    property string image
    property string thumbnail
    property string header_title
    title: header_title
    actions {
        id: context_actions
        contextualActions: [
            Kirigami.Action {
                iconName: "documentinfo"
                text: "Sidebar"
            },
            Kirigami.Action {
                iconName: "draw-star"
                onTriggered: commentlisting.refresh("hot");
                text: "Hot"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                onTriggered: commentlisting.refresh("new");
                text: "New"
            },
            Kirigami.Action {
                iconName: "labplot-xy-plot-two-axes-centered"
                onTriggered: commentlisting.refresh("rising");
                text: "Rising"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                onTriggered: commentlisting.refresh("top");
                text: "Top"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                text: "Settings"
            }
        ]
    }
    Rectangle {
        id: loading;
        Layout.alignment: Qt.AlignCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Controls.BusyIndicator {}
    }

    RiddetComponents.CommentListDelegate { id: commentDelegate }

    ListView {
        id: commentlisting
        // model: ListModel {}
        model: ListModel {}
        // delegate: Controls.ItemDelegate {
        //     Item: commentDelegate
        //     width: commentlisting.width
        // }
        delegate: Kirigami.DelegateRecycler {
            width: commentlisting.width
            sourceComponent: commentDelegate
        }
        header: ColumnLayout { 
            width: parent.width
            RowLayout {
                Layout.margins: 10
                Image {
                    source: post_page.thumbnail
                    fillMode: Image.PreserveAspectFit
                    height: 100
                }
                Kirigami.Heading {
                    Layout.fillWidth: true
                    id: post_title
                    text: post_page.header_title
                    wrapMode: Text.WordWrap        
                }
            }
            Rectangle {
                id: imageBox
                Layout.fillWidth: true
                // visible: false
                // height: 400
                color: red
                Layout.preferredHeight: 400            
                Image {
                    id: imagePost
                    visible: false
                    Layout.fillWidth: true
                    source: post_page.image
                    anchors.fill: parent
                    fillMode: Image.PreserveAspectFit
                    onStatusChanged: {
                        if (imagePost.status == Image.Ready) {     
                            let contentHeight = Math.floor(imagePost.sourceSize.height * (imagePost.width / imagePost.sourceSize.width));
                            parent.Layout.preferredHeight = contentHeight;
                            print(contentHeight);
                            imagePost.visible = true;
                        }
                    }                
                }
            }
            Video {
                id: video
                visible: false
                // width : 800
                Layout.fillWidth: true
                height : 400
                source: post_page.video

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        video.play()
                    }
                }
                
                onStatusChanged: {
                    console.log("video status changed")
                }
                // Keys.onSpacePressed: video.playbackState == MediaPlayer.PlayingState ? video.pause() : video.play()
                // Keys.onLeftPressed: video.seek(video.position - 5000)
                // Keys.onRightPressed: video.seek(video.position + 5000)
            }            
            WebEngineView {
                id: webViewer
                visible: false
                Layout.fillWidth: true
                height: 400
                onLoadingChanged: {
                    switch (loadRequest.status)
                    {
                    case WebEngineView.LoadSucceededStatus:
                        // parent.Layout.preferredHeight = 400;
                        webViewer.Layout.preferredHeight = 400;
                        print(webViewer.contentsSize);
                        webViewer.runJavaScript("document.querySelector('iframe').getAttribute('height')", function(result) { console.log("WINDOWHEIGHT " + result); });

                        break;
                    case WebEngineView.LoadStartedStatus:
                    case WebEngineView.LoadStoppedStatus:
                        break
                    case WebEngineView.LoadFailedStatus:
                        selfText.text = "Failed to load the requested video"
                        break
                    }
                }                
            }                        
            Controls.Label {
                id: authorLabel
                font.pointSize: 8
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                text: qsTr("u/")
            }         
        }
        //  header: Kirigami.ItemViewHeader {
        //      backgroundImage.source: post_page.image
        //      title: post_page.header_title
        //  }

        function refresh() {
            // console.log("load post " + post);
            commentlisting.visible = false;
            loading.visible = true;
            commentlisting.model.clear();
            RedditListing.get_comments(post).then(reddit_post_with_comments => {

                let reddit_post = reddit_post_with_comments.post;
                // root.set_preview(reddit_post);
                // if (reddit_post.is_self) {
                //     link_action.visible = false;
                // }
                // post_title.text = reddit_post.title
                post_page.header_title = reddit_post.title;
                console.log(JSON.stringify(reddit_post, null, 2));
                post_page.image = reddit_post.image || ""
                if (!reddit_post.image) {
                    post_page.thumbnail = reddit_post.thumbnail || "";
                }
                for(let comment in reddit_post_with_comments.comments) {
                    // console.log(comment);
                    commentlisting.model.append(reddit_post_with_comments.comments[comment]);
                }
                if (post_page.post_hint == "hosted:video") {
                    console.log("We're showing a video here");
                    post_page.video = reddit_post.video;
                    video.visible = true;
                    // webViewer.url = reddit_post.video;
                    // webViewer.visible = true;
                }
                loading.visible = false;
                commentlisting.visible = true;

            }).catch(e => {
                console.log(e);
            });
        }
        // delegate: commentDelegate
        footer: Kirigami.ActionToolBar {
            height: 64
            display: Controls.Button.IconOnly
            alignment: Qt.AlignCenter
            flat: true
            actions: [
                Kirigami.Action {
                    id: link_action
                    iconName: "link"
                    text: "Link"
                    onTriggered: Qt.openUrlExternally(url)
                },
                Kirigami.Action {
                    iconName: "view-preview"
                    text: "Preview"
                    onTriggered: {
                        root.show_preview();
                        // let preview_overlay = root.get_post_preview();
                        // preview_overlay.show(reddit_post)
                    }
                },
                Kirigami.Action {
                    iconName: "go-down"
                    text: "downvote"
                },
                Kirigami.Action {
                    iconName: "go-up"
                    text: "upvote"
                    onTriggered: set_subreddit("Front Page")
                },
                Kirigami.Action {
                    iconName: "edit-comment"
                    text: "comment"
                }
            ]
        }        
        Component.onCompleted: refresh()

    }   


    // ListView {
    //     id: commentlisting
    //     visible: false
    //     model: ListModel {}
    //     headerPositioning: ListView.PullBackHeader
    //     header: Kirigami.ItemViewHeader {
    //         backgroundImage.source: post_page.image
    //         title: post_page.header_title
    //     }
    //     delegate: commentDelegate
    //     // delegate: Kirigami.DelegateRecycler {
    //     //     width: commentlisting.width
    //     //     sourceComponent: commentDelegate
    //     // }
    //     Component.onCompleted: refresh()
    //     function refresh() {
    //         // console.log("load post " + post);
    //         commentlisting.visible = false;
    //         loading.visible = true;
    //         commentlisting.model.clear();
    //         RedditListing.get_comments(post).then(reddit_post_with_comments => {

    //             let reddit_post = reddit_post_with_comments.post;
    //             // root.set_preview(reddit_post);
    //             // if (reddit_post.is_self) {
    //             //     link_action.visible = false;
    //             // }
    //             for(let comment in reddit_post_with_comments.comments) {
    //                 // console.log(comment);
    //                 commentlisting.model.append(reddit_post_with_comments.comments[comment]);
    //             }
    //             loading.visible = false;
    //             commentlisting.visible = true;

    //         }).catch(e => {
    //             console.log(e);
    //         });
    //     }
    //     moveDisplaced: Transition {
    //         YAnimator {
    //             duration: Kirigami.Units.longDuration
    //             easing.type: Easing.InOutQuad
    //         }
    //     }
    // }

}
