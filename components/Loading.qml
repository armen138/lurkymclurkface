import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami

Kirigami.ScrollablePage {
    id: loading
    title: "Loading ..."
    Controls.BusyIndicator {    
    }
}
