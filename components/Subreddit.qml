import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents
import "../mjs/listing.mjs" as RedditListing

Kirigami.ScrollablePage {
    id: subreddit_page
    property string subreddit
    property string after
    property string kind: "hot"
    property string header_title
    property string banner
    title: subreddit
    actions {
        id: context_actions
        contextualActions: [
            Kirigami.Action {
                iconName: "documentinfo"
                text: "Sidebar"
            },
            Kirigami.Action {
                iconName: "draw-star"
                onTriggered: { kind = "hot"; postlisting.refresh(); }
                text: "Hot"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                onTriggered: { kind = "new"; postlisting.refresh(); }
                text: "New"
            },
            Kirigami.Action {
                iconName: "labplot-xy-plot-two-axes-centered"
                onTriggered: { kind = "rising"; postlisting.refresh(); }      
                text: "Rising"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                onTriggered: { kind = "top"; postlisting.refresh(); }
                text: "Top"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                text: "Settings"
            }
        ]
    }
    Rectangle {
        id: loading;
        Layout.alignment: Qt.AlignCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Controls.BusyIndicator {}    
    }
    RiddetComponents.PostDelegate { id: postDelegate }
    ListView {
        id: postlisting
        // add: Transition {
        //     NumberAnimation { properties: "x"; from: parent.width; duration: 1000 }
        // }
        // addDisplaced: Transition {
        //     NumberAnimation { properties: "y"; duration: 1000 }
        // }
        visible: false
        model: ListModel {}
        // delegate: postDelegate
        delegate: Kirigami.DelegateRecycler {
            width: postlisting.width
            sourceComponent: postDelegate
        }
        headerPositioning: ListView.PullBackHeader
        header: Kirigami.ItemViewHeader {
            backgroundImage.source: banner
            title: subreddit_page.header_title
        }
        Component.onCompleted: {
            refresh();
        }
        function refresh() {
            // kind = kind || "hot";
            if (kind !== "hot") {
                title = `${subreddit}[${kind}]`;
            }
            postlisting.visible = false
            loading.visible = true
            postlisting.model.clear();
            RedditListing.get_subreddit(subreddit_page.subreddit, kind).then(function(data) {
                for (let item of data.items) {
                    if(item) postlisting.model.append(item);
                }
                after = data.after;
                loading.visible = false;
                postlisting.visible = true;
            });
        }
        function next_page() {
            RedditListing.get_subreddit(subreddit_page.subreddit, kind, after).then(function(data) {
                for (let item of data.items) {
                    if(item) postlisting.model.append(item);
                }
                after = data.after;
            });
        }
        onFlickStarted: {
            console.log(postlisting.verticalOvershoot);
            if(postlisting.verticalOvershoot < -100) {
                console.log("Refresh");
                refresh();
            }
            if(postlisting.verticalOvershoot > 100) {
                console.log("Paginate");
                next_page();
            }
        }        
        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }
}
