import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import QtWebKit 3.0
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents
import "../mjs/listing.mjs" as RedditListing

Kirigami.ScrollablePage {
    id: post_page
    property string url
    property string post
    property string header_title
    title: header_title.substr(0, 16)
    actions {
        id: context_actions
        contextualActions: [
            Kirigami.Action {
                iconName: "documentinfo"
                text: "Sidebar"
            },
            Kirigami.Action {
                iconName: "draw-star"
                onTriggered: commentlisting.refresh("hot");
                text: "Hot"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                onTriggered: commentlisting.refresh("new");
                text: "New"
            },
            Kirigami.Action {
                iconName: "labplot-xy-plot-two-axes-centered"
                onTriggered: commentlisting.refresh("rising");                
                text: "Rising"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                onTriggered: commentlisting.refresh("top");                
                text: "Top"
            },
            Kirigami.Action {
                iconName: "games-config-options"
                text: "Settings"
            }
        ]
    }
    Rectangle {
        id: loading;
        Layout.alignment: Qt.AlignCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Controls.BusyIndicator {}    
    }
    Component.onCompleted: refresh()
    ColumnLayout {
        spacing: Kirigami.Units.largeSpacing * 5
        Layout.preferredWidth: Kirigami.Units.gridUnit * 25
        Kirigami.Heading {
            Layout.fillWidth: true
            text: header_title
            wrapMode: Text.WordWrap
        }
        Controls.Label {
            id: subredditLabel
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: ""
        }
        Rectangle {
            id: contentContainer
            color: "transparent"
            Layout.fillWidth: true
            Image {
                visible: false  
                anchors.fill: parent
                id: imagePost
                fillMode: Image.PreserveAspectFit
                onStatusChanged: {
                    if (imagePost.status == Image.Ready) {     
                        let contentHeight = Math.floor(imagePost.sourceSize.height * (imagePost.width / imagePost.sourceSize.width));
                        parent.Layout.preferredHeight = contentHeight;
                        imagePost.visible = true;
                    }
                }
            }
            WebView {
                id: webViewer
                visible: false
                anchors.fill: parent
                onLoadingChanged: {
                    switch (loadRequest.status)
                    {
                    case WebView.LoadSucceededStatus:
                        parent.Layout.preferredHeight = 400;
                        break;
                    case WebView.LoadStartedStatus:
                    case WebView.LoadStoppedStatus:
                        break
                    case WebView.LoadFailedStatus:
                        selfText.text = "Failed to load the requested video"
                        break
                    }
                }                
            }    
        } 
        RowLayout {
            Controls.Button {
                id: up_button
                text: "\ue801"
                flat: true
            }
            Controls.Button {
                id: down_button
                text: "\ue800"
                flat: true
            }    
            Controls.Button {
                id: comment_button
                text: "\ue80c"
                flat: true
            }                        
        }
        RiddetComponents.CommentDelegate { id: commentDelegate }
        RowLayout {
            ListView {
                id: commentlisting
                visible: true
                model: ListModel {}
                delegate: Kirigami.DelegateRecycler {
                    width: commentlisting.width
                    sourceComponent: commentDelegate
                }
                moveDisplaced: Transition {
                    YAnimator {
                        duration: Kirigami.Units.longDuration
                        easing.type: Easing.InOutQuad
                    }
                }
            }
        }
    }    
    function refresh() {
        console.log("load post " + post);
        RedditListing.get_comments(post).then(reddit_post_with_comments => {
            let reddit_post = reddit_post_with_comments.post;
            for(let comment in reddit_post_with_comments.comments) {
                // console.log(comment);
                commentlisting.model.append(reddit_post_with_comments.comments[comment]);
            }
            loading.visible = false;
            subredditLabel.text = `r/${reddit_post.subreddit} - ${reddit_post.domain}`;
            url = reddit_post.url;
            console.log(reddit_post.post_hint);
            switch(reddit_post.post_hint) {
                case "image":
                    imagePost.source = reddit_post.url;
                    break;
                case "self":
                    selfText.text = reddit_post.selftext;
                    selfText.visible = true;
                    console.log("set self text", reddit_post.selftext);
                    break;
                case "rich:video":
                    console.log(reddit_post.url);
                    if(reddit_post.youtube_video) {
                        webViewer.url = "../html/yt_player.html?" + reddit_post.youtube_video;
                    }
                    if(reddit_post.gfy_embed) {
                        webViewer.url = reddit_post.gfy_embed;
                    }
                    webViewer.visible = true;
                    break;
                case "hosted:video":
                    webViewer.url = reddit_post.video;
                    webViewer.visible = true;
                    break;
                case "link":
                    if (reddit_post.tweet) {
                        webViewer.loadHtml(reddit_post.tweet);
                        try {
                            webViewer.runJavaScript("window.innerHeight", data => {
                                console.log(data);
                            });
                        } catch(e) {
                            console.log(e);
                        }
                        webViewer.visible = true;
                        contentContainer.Layout.preferredHeight = reddit_post.height;
                        break;
                    }
                default:
                    console.log(reddit_post.url);
                    if(reddit_post.selftext) {
                        selfText.text = reddit_post.selftext;
                        selfText.visible = true;
                        console.log("set self text", reddit_post.selftext);
                        break;
                    }
                    imagePost.source = reddit_post.thumbnail;
                    // imagePost.visible = true;
            }            
        });
    }
}
