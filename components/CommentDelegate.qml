import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents

Component {
    id: commentDelegate
    Item {
        id: model.id
        implicitHeight: model.kind != "more" ? card.implicitHeight + delegateLayout.implicitHeight : 64

        Kirigami.Card {
            anchors.leftMargin: 5 * model.level
            anchors.fill: parent
            id: card
            //NOTE: never put a Layout as contentItem as it will cause binding loops
            //SEE: https://bugreports.qt.io/browse/QTBUG-66826
            contentItem: Item {
                id: content_item

                RowLayout {
                    id: delegateLayout
                    anchors {
                        left: parent.left
                        top: parent.top
                        right: parent.right
                        //IMPORTANT: never put the bottom margin
                    }
                    Controls.Label {
                        visible: (model.kind != "more")
                        Layout.fillWidth: true
                        wrapMode: Text.WordWrap
                        text: model.body
                    }       
                }
            }
            Rectangle {
                visible: (model.kind != "more")
                anchors {
                    left: card.left
                    top: card.top
                }                    
                width: 5
                // height: top_card.height
                color: model.color
            }
            header: RowLayout { 
                visible: (model.kind != "more")
                Controls.Label {
                    text: (model.kind == "more") ? " load more comments..." : " [" + model.score + "]"
                }
                Item {
                    visible: (model.kind != "more")
                    Layout.fillWidth: true
                }
                Controls.Button {
                    visible: (model.kind != "more")
                    text: model.author != "[deleted]" ? "u/" + model.author : model.author
                    onClicked: {
                        console.log(JSON.stringify(model.kind));
                    }
                    flat: true
                }
            }
            actions: [
                Kirigami.Action {
                    visible: (model.kind != "more")
                    iconSource: "../icons/chevron-bottom.svg"
                    expandible: true
                },
                Kirigami.Action {
                    visible: (model.kind != "more")
                    iconSource: "../icons/chevron-top.svg"
                    // onTriggered: set_subreddit("Front Page")
                    expandible: true
                },
                Kirigami.Action {
                    visible: (model.kind != "more")
                    iconSource: "../icons/comment-square.svg"
                    expandible: true
                },
                Kirigami.Action {
                    visible: (model.kind == "more")
                    iconSource: "../icons/loop-circular.svg"
                    text: "Load " + model.count + " comment" + (model.count > 1 ? "s" : "")
                    expandible: true
                }
            ]
            // footer: RowLayout {
            //     id: footsies
            //     height: 24
                
            //     Controls.Button {
            //         visible: (model.kind != "more")
            //         text: model.author != "[deleted]" ? "u/" + model.author : model.author
            //         onClicked: {
            //             console.log(JSON.stringify(model.kind));
            //         }
            //         flat: true
            //     }                  
            //     Kirigami.ActionToolBar {
            //         id: action_toolbar
            //         display: Controls.Button.TextBesideIcon
            //         alignment: Qt.AlignRight
            //         visible: (model.kind != "more")
            //         // flat: true
            //         actions: [
            //             Kirigami.Action {
            //                 iconSource: "../icons/comment-square.svg"
            //             },
            //             Kirigami.Action {
            //                 iconSource: "../icons/chevron-bottom.svg"
            //             },
            //             Kirigami.Action {
            //                 text: model.score
            //             },                    
            //             Kirigami.Action {
            //                 iconSource: "../icons/chevron-top.svg"
            //             }
            //         ]
            //     }
            // }
        }
    }    
}
