import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents

Component {
    id: commentDelegate
    Kirigami.BasicListItem {
        id: listItem
        scale: model.folded ? 0.0 : 1.0
        contentItem: RowLayout {
            Rectangle {
                id: indent_spacer
                Layout.fillHeight: true
                color: "transparent"
                implicitWidth: model.indent
            }
            Rectangle {
                id: indent_bar
                width: 10
                Layout.fillHeight: true
                color: model.color
            }

            ColumnLayout {
                id: inner_content
                RowLayout {
                    id: user_layout
                    visible: model ? (model.kind != "more") : true
                    Controls.ToolButton {
                        id: fold_button
                        visible: true
                        onClicked: function() {
                            fold_button.visible = false;
                            unfold_button.visible = true;
                            console.log(model.id);
                            // root.hide_replies([ model.id ]);
                            console.log(commentlisting.model.count);
                            for (let i = 0; i < commentlisting.model.count; i++) {
                                let item = commentlisting.model.get(i);
                                console.log(item);
                                if (model.get_reply_ids().indexOf(item.id) > 0) { //} == model.id) {
                                    item.folded = true;
                                    console.log("item found");
                                }
                            }
                            //     let child = model.children[i];
                            //     console.log("hide", child);
                            //     // globalObject()[child].visible = false;
                            // }
                        }
                        text:""
                        icon.source: "../icons/caret-bottom.svg"
                        font.pointSize: 8
                    }
                    Controls.ToolButton {
                        id: unfold_button
                        visible: false
                        onClicked: function() {
                            fold_button.visible = true;
                            unfold_button.visible = false;
                        }
                        text:""
                        icon.source: "../icons/caret-right.svg"
                        font.pointSize: 8
                    }
                    Controls.ToolButton {
                        text: "u/" + ( model && model.author) || ""
                        font.pointSize: 8
                        onClicked: root.set_userpage(model.author)
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    Controls.Label {
                        text: model.when //"hours ago"
                        font.pointSize: 8
                    }
                }                
                RowLayout {
                    Controls.Label {
                        id: content_label
                        visible: model ? (model.kind != "more") : true
                        Layout.fillWidth: true
                        wrapMode: Text.WordWrap
                        text: model && model.body || "title"
                        color: listItem.checked || (listItem.pressed && !listItem.checked && !listItem.sectionDelegate) ? listItem.activeTextColor : listItem.textColor
                    }
                }
                RowLayout {
                    id: row_layout
                    Controls.ToolButton {
                        visible: model ? (model.kind == "more") : true
                        onClicked: model.submit()
                        text: model ? "Load " + model.count + " comment" + (model.count > 0 ? "s" : "") : ""
                        icon.source: "../icons/loop-circular.svg"
                        font.pointSize: 8
                    }
                    Item {
                        Layout.fillWidth: true
                    }
                    Controls.ToolButton {
                        visible: model ? (model.kind != "more") : true
                        onClicked: model.submit()
                        text: model ? model.score : ""
                        icon.source: "../icons/chevron-top.svg"
                        font.pointSize: 8
                    }
                    Controls.ToolButton {
                        visible: model ? (model.kind != "more") : true
                        onClicked: model.submit()
                        text: ( model && model.num_comments || 0)
                        icon.source: "../icons/comment-square.svg"
                        font.pointSize: 8
                    }
                }
            }
        }
        // Component.onCompleted: function() {
        //     indent.implicitWidth = model.indent;
        //     indent.color = model.color;
        //     console.log(model.indent, model.author, model.color, model.depth)
        // }
        // onClicked: showPassiveNotification("preview")
        onClicked: function() {
            console.log(model.depth, model.kind, model.author)
            // console.log(JSON.stringify(model, null, 2));
            // indent.implicitWidth = model.indent;
            // indent.color = model.color;
            // console.log(model.indent, model.author, model.color, model.depth)            
        }
    }
}
