import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "." as RiddetComponents
import "../mjs/listing.mjs" as RedditListing

Kirigami.ScrollablePage {
    id: front_page
    property string subreddit: "Front Page"
    property string after
    title: "Front Page"
    actions {
        id: context_actions
        contextualActions: [
            Kirigami.Action {
                iconSource: "../icons/loop-circular.svg"
                text: "Refresh"
                onTriggered: postlisting.refresh();
            },
            Kirigami.Action {
                iconSource: "../icons/cog.svg"
                text: "Settings"
            }
        ]
    }
    Timer {
        id: timer
        function setTimeout(cb, delayTime) {
            timer.interval = delayTime;
            timer.repeat = false;
            timer.triggered.connect(cb);
            timer.triggered.connect(function release () {
                timer.triggered.disconnect(cb); // This is important
                timer.triggered.disconnect(release); // This is important as well
            });
            timer.start();
        }
    }    
    Rectangle {
        id: loading;
        Layout.alignment: Qt.AlignCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        Controls.BusyIndicator {}    
    }    
    RiddetComponents.PostDelegate { id: postDelegate }
    ListView {
        id: postlisting
        visible: false
        model: ListModel {}
        delegate: Kirigami.DelegateRecycler {
            width: postlisting.width
            sourceComponent: postDelegate
        }
        Component.onCompleted: timer.setTimeout(refresh(), 1000)
        function refresh() {
            postlisting.visible = false;
            loading.visible = true;
            postlisting.model.clear();
            RedditListing.get_front_page().then(function(data) {
                console.log(data);
                // postlisting.model.append(data.items[0])
                // postlisting.model.append(data.items[1])

                for (let item of data.items) {
                    postlisting.model.append(item);
                }
                after = data.after;
                loading.visible = false;
                postlisting.visible = true;
            });
        }
        function next_page() {
            RedditListing.get_front_page(after).then(function(data) {
                for (let item of data.items) {
                    if(item) postlisting.model.append(item);
                }
                after = data.after;
            });
        }
        onFlickStarted: {
            console.log(postlisting.verticalOvershoot);
            if(postlisting.verticalOvershoot < -100) {
                console.log("Refresh");
                refresh();
            }
            if(postlisting.verticalOvershoot > 100) {
                console.log("Paginate");
                next_page();
            }
        }
        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }
    }
}
