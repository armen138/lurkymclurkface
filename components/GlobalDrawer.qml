import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami

Kirigami.GlobalDrawer {
    title: "LurkyMcLurkFace"
    titleIcon: "res://../icons/dashboard.svg"
    actions: [
        Kirigami.Action {
            text: "View"
            iconName: "view-list-icons"
            Kirigami.Action {
                text: "action 1"
            }
            Kirigami.Action {
                text: "action 2"
            }
            Kirigami.Action {
                text: "action 3"
            }
        },
        Kirigami.Action {
            text: "action 3"
        },
        Kirigami.Action {
            text: "action 4"
        }
    ]
}