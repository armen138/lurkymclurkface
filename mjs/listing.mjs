// .import QtQuick.Controls 2.14 as Controls;
import RedditPost from "redditpost.mjs";
import RedditComment from "redditcomment.mjs";

const CURRENT_LISTING = "pinephone";

function _get_json(url) {
    let promise = new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                try {
                    resolve(JSON.parse(xhr.responseText));
                } catch(e) {
                    // console.log(xhr.responseText);
                    reject(e);
                }
            }
        }
        xhr.onerror = function(e) {
            reject(e);
        }
        xhr.open("GET", url);
        xhr.send();    
    });
    return promise;
}
export function get_sidebar(subreddit) {
    let promise = new Promise((resolve, reject) => {
        _get_json(`https://reddit.com/r/${subreddit}/about.json`).then(data => {
            resolve(data.data);
        });
    
    });
    return promise;    
}

function flatten_comments(comments, level) {
    level = level || 0;
    let flattened = [];
    console.log("top level comments: " + comments.length);
    for (let comment of comments) {
        comment.data.level = level;
        flattened.push(comment);
        console.log(Object.keys(comment));
        if(comment.data.replies) {
            console.log(comment.data.replies.data.children.length);
            flattened = flattened.concat(flatten_comments(comment.data.replies.data.children, level + 1));
            comment.data.reply_ids = [];
            for (var i = 0; i < flattened.length; i++) {
                comment.data.reply_ids.push(flattened[i].data.id);
            }
        }
    }
    console.log("flattened comments: " + flattened.length);
    return flattened;
}
export function get_comments(article) {
    let id = article;
    console.log(`https://reddit.com/comments/${article}.json`);
    let promise = new Promise((resolve, reject) => {
        _get_json(`https://reddit.com/comments/${article}.json`).then(data => {
            try {
                let flat = flatten_comments(data[1].data.children);
                let post = new RedditPost(data[0].data.children[0]);
                // let comments = data[1].data.children.map(item => new RedditComment(item));
                let comments = flat.map(item => new RedditComment(item));
                // console.log(JSON.stringify(comments));
                // let items = data.data.children.map(item => new RedditComment(item));    
                // console.log(JSON.stringify(comments));    
                resolve({ post: post, comments: comments });    
            } catch(e) {
                console.log(e);
                reject(e);
            }
        });
    });
    return promise;
}
export function get_subreddit(subreddit, kind, after) {
    after = after || "";
    kind = kind || "hot";
    let promise = new Promise((resolve, reject) => {
        console.log("paginate past " + after);
        _get_json(`https://reddit.com/r/${subreddit}/${kind}.json?after=${after}`).then(data => {
            // print(data);
            let items = data.data.children.map(item => new RedditPost(item));        
            resolve({ items: items, after: data.data.after });
        });
    });
    return promise;
}
export function get_user(user, kind, after) {
    after = after || "";
    kind = kind || "hot";
    let promise = new Promise((resolve, reject) => {
        console.log("paginate past " + after);
        _get_json(`https://reddit.com/u/${user}/${kind}.json?after=${after}`).then(data => {
            // print(data);
            let items = data.data.children.map(item => new RedditPost(item));        
            resolve({ items: items, after: data.data.after });
        });
    });
    return promise;
}
export function get_trending() {
    let url = "https://www.reddit.com/api/trending_subreddits.json";
    let promise = new Promise((resolve, reject) => {
        _get_json(url).then(data => {
            // print(JSON.stringify(data));
            try {
                let items = data.data.children.map(item => new RedditPost(item));        
                resolve(items);    
            } catch(e) {
                console.log(e);
                reject(e);
            }
        });
    });
    return promise;
}
export function get_front_page(after) {
    after = after || "";
    let url = `https://reddit.com/hot.json?after=${after}`;
    let promise = new Promise((resolve, reject) => {
        _get_json(url).then(data => {
            // print(JSON.stringify(data));
            try {
                let items = data.data.children.map(item => new RedditPost(item));        
                resolve({ items: items, after: data.data.after });    
            } catch(e) {
                console.log(e);
                reject(e);
            }
        });
    });
    return promise;
}
