function get_yt_id(url) {
    let tokens = url.split("/"); // youtub.be
    let full_url_tokens = url.split("?v=") // youtube
    let yt_id = tokens.pop().split("?")[0];
    if(full_url_tokens.length > 1) {
        yt_id = full_url_tokens[1];
    }
    return yt_id;
}

function get_gfy_embed(url) {
    let ifr_url = url.split('/');
    let gfy_id = ifr_url.pop();
    ifr_url.push("ifr");
    ifr_url.push(gfy_id);
    return ifr_url.join("/");    
}

function html(data) {
    return data.replace(/\&lt;/g, "<")
                .replace(/\&gt;/g, ">")
                .replace(/\&amp;/g, "&")
                .replace(/\&nbsp;/g, " ")
}
class RedditPost {
    constructor(raw_post) {
        let keys = [
            "id",
            "num_comments",
            "title",
            "author",
            "domain",
            "selftext",
            "is_self",
            "score",
            "ups",
            "url",
            "downs",
            "thumbnail",
            "created",
            "subreddit",
            "post_hint",
            "domain",
            "header_title"
        ];
        for(let i = 0; i < keys.length; i++) {
            let key = keys[i];
            if(raw_post.data[key] !== null && raw_post.data[key] !== undefined && raw_post.data[key] !== false) {
                this[key] = raw_post.data[key];
                if(key == "thumbnail") {
                    console.log(key, raw_post.data[key]);
                }
            }
        }
        try {
            this.video = raw_post.data.media.reddit_video.fallback_url;
        } catch(e) {}
        if(this.url.indexOf("youtu") !== -1) {
            this.youtube_video = get_yt_id(this.url);
        }
        if(this.url.indexOf("gfycat.com") !== -1) {
            this.gfy_embed = get_gfy_embed(this.url);
        }
        if(this.url.indexOf("twitter.com" !== -1) && raw_post.data.media_embed.content) { 
            console.log(this.url);
            // this.tweet = "data:text/html;charset=utf-8," + html(raw_post.data.media_embed.content);
            this.tweet = html(raw_post.data.media_embed.content);
            this.height = raw_post.data.media_embed.height;
            console.log(this.tweet);
        }
        if(this.post_hint == "image") {
            this.image = this.url;
        }
        switch(this.thumbnail) {
            case "self":
                this.thumbnail = "../icons/person-8x.png";
                break;
            case "nsfw":
                this.thumbnail = "../icons/warning-8x.png";
                break;
            case "default":
                this.thumbnail = "../icons/image-8x.png";
                break;
            default:
                break;        
        }        
    }
    submit() {
        console.log("submit");
    }
}

export default RedditPost;