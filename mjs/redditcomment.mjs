import moment from "moment.mjs";
// import "moment.mjs" as Moment

let colors = [
    "transparent",
    "#346524",
    "#d04648",
    "#757161",
    "#597dce",
    "#d27d2c",
    "#8595a1",
    "#6daa2c",
    "#d2aa99",
    "#6dc2ca",
    "#dad45e",
    "#deeed6",
    "#140c1c",
    "#442434",
    "#30346d",
    "#4e4a4e",
    "#854c30"
];

class RedditComment {
    constructor(raw_post) {
        let keys = [
            "id",
            "body",
            "reply_ids",
            "author",
            "score",
            "ups",
            "all_awards",
            "count",
            "depth"
        ];
        for(let i = 0; i < keys.length; i++) {
            let key = keys[i];
            if(raw_post.data[key] !== null && raw_post.data[key] !== undefined && raw_post.data[key] !== false) {
                this[key] = raw_post.data[key];
            }
        }
        this.when = moment(raw_post.data.created_utc * 1000).fromNow()
        this.folded = false;
        if(this.reply_ids) {
            console.log("children", this.reply_ids);
        }
        // if(!this.author) {
        //     console.log(JSON.stringify(raw_post));
        // }
        this.kind = raw_post.kind;
        // if(raw_post.data && raw_post.data.count) {
        //     this.count = raw_post.data.count;
        // }
        this.color = colors[this.depth];
        this.indent = this.depth * 10;
        // this.level = level || 0;
        this.get_reply_ids = function() {
            return this.reply_ids;
        };
    }

    submit() {
        console.log("submit");
    }
}

export default RedditComment;