# LurkyMcLurkFace

A convergent Reddit client for Linux using Kirigami/QML

TODO: 

* Add preview and link actions to post view
* Add account login
* Enable upvotes and downvotes
* Add comments
* Figure out what is required to preview video on mobile platforms
* Add user-view
* Add sidebar-view
* Add favorites
* Add styling for posts "marked as read"
* Multireddits
* Evaluate if building for android is feasable and/or desirable

... in other words, it's early days and things will be wonky.

## Icons

Icons are OPEN-ICONIC, from https://github.com/iconic/open-iconic