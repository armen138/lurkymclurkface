import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.5 as Kirigami
import "components" as RiddetComponents
import "mjs/listing.mjs" as RedditListing


Kirigami.ApplicationWindow {
    id: root
    width: 640
    height: 960
    globalDrawer: RiddetComponents.GlobalDrawer { id: global_drawer }
    contextDrawer: Kirigami.ContextDrawer { id: contextDrawer }
    pageStack.initialPage: RiddetComponents.Frontpage { id: frontpage }
    // pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.TabBar

    FontLoader { id: fontello; source: "fonts/fontello.ttf" }

    RiddetComponents.Preview {  id: previewSheet }

    function find_page(name) {
        for(let page in pageStack.items) {
            if (pageStack.items[page].subreddit === name ||
                pageStack.items[page].post === name) {
                return pageStack.items[page];
            }
        }
        return null;
    }
    function set_userpage(name) {
        // console.log(JSON.stringify(Object.keys(pageStack.children)));
        let existing_page = find_page("u/" + name); //pageStack.children.filter(page => page.subreddit === name);
        // let existing_page = pageStack.find(function(page) { return page.subreddit === name });
        console.log(existing_page);
        if(existing_page) {
            pageStack.pop(existing_page);
        } else {
            RedditListing.get_user(name).then(data => {
                let sbr_component = Qt.createComponent("components/Subreddit.qml");
                let sbr = sbr_component.createObject(root, {
                    id: "user_" + name, 
                    subreddit: "u/" + name, 
                    header_title: data.title || data.header_title || data.display_name_prefixed,
                    banner: data.mobile_banner_img || data.banner_img || data.header_img || data.banner_background_image
                });
                pageStack.push(sbr);
            });
        }
    }    
    function set_subreddit(name) {
        // console.log(JSON.stringify(Object.keys(pageStack.children)));
        let existing_page = find_page(name); //pageStack.children.filter(page => page.subreddit === name);
        // let existing_page = pageStack.find(function(page) { return page.subreddit === name });
        console.log(existing_page);
        if(existing_page) {
            pageStack.pop(existing_page);
        } else {
            RedditListing.get_sidebar(name).then(data => {
                let sbr_component = Qt.createComponent("components/Subreddit.qml");
                let sbr = sbr_component.createObject(root, {
                    id: "subreddit_" + name, 
                    subreddit: name, 
                    header_title: data.title || data.header_title || data.display_name_prefixed,
                    banner: data.mobile_banner_img || data.banner_img || data.header_img || data.banner_background_image
                });
                pageStack.push(sbr);
            });
        }
    }

    function set_post(model) {
        let existing_page = find_page(model.id);
        console.log(existing_page);
        console.log(model);
        if(existing_page) {
            pageStack.pop(existing_page);
        } else {
            // RedditListing.get_sidebar(name).then(data => {
                let sbr_component = Qt.createComponent("components/PostComments.qml");
                let sbr = sbr_component.createObject(root, {
                    id: "post_" + model.id, 
                    // id: "post_comments",
                    post: model.id,
                    image: model.thumbnail,
                    header_title: model.title
                });
                pageStack.push(sbr);
            // });
        }
    }
    function set_preview(post) {
        previewSheet.set_preview(post);
    }
    function show_preview() {
        previewSheet.show();
    }
    function get_post_preview(post_id) {
        let preview_component = Qt.createComponent("components/Preview.qml");
        let preview = preview_component.createObject(root, { id: "postPreview", post: post_id });
        return preview;
    }
    function set_front_page() {
        let frontpage_component = Qt.createComponent("components/Frontpage.qml");
        let frontpage = frontpage_component.createObject(root, { id: "frontpage" });
        pageStack.push(frontpage);
    }
    function hide_replies(reply_ids) {
        console.log("hide", reply_ids.length, reply_ids);
        console.log(post_comments.model.count);
        // for(var i = 0; i < reply_ids.length; i++) {
        //     var reply_id = reply_ids[i];
        //     getElementById(reply_id).visible = false;
        // }

    }
    footer: Kirigami.ActionToolBar {
        height: 64
        display: Controls.Button.IconOnly
        alignment: Qt.AlignCenter
        flat: true
        actions: [
            Kirigami.Action {
                iconSource: "icons/list.svg"
                text: "My Subreddits"
            },                
            Kirigami.Action {
                iconSource: "icons/home.svg"
                text: "Home"
                onTriggered: set_subreddit("Front Page")
            },
            Kirigami.Action {
                iconSource: "icons/magnifying-glass.svg"
                text: "Search"
            },
            Kirigami.Action {
                iconSource: "icons/inbox.svg"
                text: "Inbox"
            },
            Kirigami.Action {
                iconSource: "icons/person.svg"
                text: "Account"
            }                                   
        ]
    }   
}
